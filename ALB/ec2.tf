
data "template_file" "data" {
  template = "${file("install.tpl")}"
}


resource "aws_instance" "web" {
  ami                    = "ami-0cff7528ff583bf9a"
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.subnet-2.id
  iam_instance_profile   = "EC2Profile"
  associate_public_ip_address = false
  vpc_security_group_ids = [aws_security_group.app_sg.id]
  root_block_device {
    volume_size = 20
    volume_type = "gp2"
  }
  user_data = "${data.template_file.data.rendered}"

  tags = {
    Name = "Demo Instance-1"
  }

  depends_on = [
    aws_nat_gateway.example
  ]
}

resource "aws_instance" "web-2" {
  ami                    = "ami-0cff7528ff583bf9a"
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.subnet-4.id
  iam_instance_profile   = "EC2Profile"
  associate_public_ip_address = false
  vpc_security_group_ids = [aws_security_group.app_sg.id]
  root_block_device {
    volume_size = 20
    volume_type = "gp2"
  }
  user_data = "${data.template_file.data.rendered}"

  tags = {
    Name = "Demo Instance-2"
  }
  depends_on = [
    aws_nat_gateway.example
  ]
}



resource "aws_iam_instance_profile" "EC2Profile" {
  name = "EC2Profile"
  role = "${aws_iam_role.EC2Role.name}"
}

resource "aws_iam_role" "EC2Role" {
  name = "EC2Profile"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "ec2.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

  tags = {
    name = "EC2Profile"
  }
}
