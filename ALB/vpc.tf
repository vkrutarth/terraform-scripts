# # Create a VPC


resource "aws_vpc" "aws_demo" {
  cidr_block                       = "10.0.0.0/16"
  enable_dns_hostnames             = true
  enable_dns_support               = true
  enable_classiclink_dns_support   = true
  assign_generated_ipv6_cidr_block = false
  tags = {
    Name = "aws_test"
    Tag2 = "new tag"
  }
}

resource "aws_subnet" "subnet" {
  cidr_block              = "10.0.1.0/24"
  vpc_id                  = aws_vpc.aws_demo.id
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "aws-subnet-1-public"
  }
}

resource "aws_subnet" "subnet-2" {
  cidr_block              = "10.0.2.0/24"
  vpc_id                  = aws_vpc.aws_demo.id
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = false

  tags = {
    Name = "aws-subnet-2-private"
  }
}

resource "aws_subnet" "subnet-3" {
  cidr_block              = "10.0.3.0/24"
  vpc_id                  = aws_vpc.aws_demo.id
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "aws-subnet-3-public"
  }
}

resource "aws_subnet" "subnet-4" {
  cidr_block              = "10.0.4.0/24"
  vpc_id                  = aws_vpc.aws_demo.id
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = false

  tags = {
    Name = "aws-subnet-4-private"
  }
}

resource "aws_internet_gateway" "main-gateway" {
  vpc_id = aws_vpc.aws_demo.id

  tags = {
    Name = "internet-gateway"
  }
}

resource "aws_route_table" "table-1" {
  vpc_id = aws_vpc.aws_demo.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-gateway.id
  }

  tags = {
    Name = "table-1"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet.id
  route_table_id = aws_route_table.table-1.id
}



resource "aws_route_table_association" "c" {
  subnet_id      = aws_subnet.subnet-3.id
  route_table_id = aws_route_table.table-1.id
}


resource "aws_eip" "test_eip" {
  vpc              = true
}
resource "aws_nat_gateway" "example" {
  allocation_id = aws_eip.test_eip.id
  subnet_id     = aws_subnet.subnet.id

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.main-gateway]
}

resource "aws_route_table" "table-2" {
  vpc_id = aws_vpc.aws_demo.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.example.id
  }

  tags = {
    Name = "table-2"
  }
}


resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.subnet-2.id
  route_table_id = aws_route_table.table-2.id
}



resource "aws_route_table_association" "d" {
  subnet_id      = aws_subnet.subnet-4.id
  route_table_id = aws_route_table.table-2.id
}
