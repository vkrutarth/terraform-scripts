resource "aws_lb_target_group" "test_lb_tg" {
  name     = "tf-example-lb-tg"
  port     = 80
  target_type = "instance"
  protocol = "HTTP"
  vpc_id   = aws_vpc.aws_demo.id

  health_check {
    port = 80
    matcher = 200
  }
}

resource "aws_lb" "test_lb" {
  name               = "test-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb_sg.id]
  subnets            = [aws_subnet.subnet.id,aws_subnet.subnet-3.id]

  enable_deletion_protection = false

  tags = {
    ent-serviceprofile = "internet"
  }
}

resource "aws_lb_listener" "test-listener" {
  load_balancer_arn = aws_lb.test_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.test_lb_tg.arn
  }
}

resource "aws_lb_target_group_attachment" "test" {
  target_group_arn = aws_lb_target_group.test_lb_tg.arn
  target_id        = aws_instance.web.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "test-2" {
  target_group_arn = aws_lb_target_group.test_lb_tg.arn
  target_id        = aws_instance.web-2.id
  port             = 80
}

output "dns_name" {
  value       = aws_lb.test_lb.dns_name
}

