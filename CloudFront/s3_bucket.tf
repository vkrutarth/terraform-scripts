resource "aws_s3_bucket" "cd_bucket" {
  bucket = "test-cloudfront-terraform-01"
  tags = {
    Name = "test-cloudfront-terraform"
  }
}

resource "aws_s3_bucket_website_configuration" "cd_bucket" {
  bucket = aws_s3_bucket.cd_bucket.bucket

  index_document {
    suffix = "index.html"
  }
  error_document {
    key = "error.html"
  }
}
resource "aws_s3_bucket_acl" "cd_bucket" {
  bucket = aws_s3_bucket.cd_bucket.id
  acl    = "private"
}

resource "aws_s3_object" "cd_file" {
  bucket = aws_s3_bucket.cd_bucket.id
  key    = "index.html"
  source = "index.html"
  content_type = "text/html"
}




